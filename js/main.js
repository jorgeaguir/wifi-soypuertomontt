var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: 'ofbHHPElpuc',
        playerVars: { 'controls': 0, 'autoplay': 0 },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}
function onPlayerReady(event) {
    //event.target.playVideo();
}
var done = false;
function onPlayerStateChange(event) {
    if (event.data == 0) {
        closeVideoFrame();
    }
}
function stopVideo() {
    player.events.onReady.stopVideo();
}

function closeVideoFrame() {
    window.location.replace('http://www.soychile.cl/puerto-montt/')
}

var videoFrame = document.querySelector('.player');
document.getElementById('leForm').addEventListener('submit', function (e) {
    console.log('submit')
    //e.preventDefault();
    
    if (!videoFrame) {
        return
    }

    /* player.playVideo();
    let timer = player.getDuration();
    let timerStr = document.querySelector('.timer');
    timerStr.innerText = timer;
    setInterval(function () {
        timer = timer - 1;
        timerStr.innerText = timer;
    },1000);
    
    videoFrame.style.display = "flex"; */
    
    document.querySelector('#leForm button').disabled = true;
})

// Formatear rut
function formatRut(val) {
    var rutField = document.querySelector('#rut');
    let rut = typeof val === 'string' ? val.replace(/^0+|[^0-9kK]+/g, '').toUpperCase() : '';

    let result = rut.slice(-4, -1) + '-' + rut.substr(rut.length - 1);
    for (var i = 4; i < rut.length; i += 3) {
        result = rut.slice(-3 - i, -i) + '.' + result
    }
    rutField.value = result;
}