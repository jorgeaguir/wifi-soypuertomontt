<?php
if (isset($_POST['mac'])) {
session_start();
$mac = $_POST['mac'];
$ip = $_POST['ip'];
$username = $_POST['username'];
$link_login = $_POST['link-login'];
$link_orig = $_POST['link-orig'];
$error = $_POST['error'];
$chap_id =  $_POST['chap-id'];
$chap_challenge =  $_POST['chap-challenge'];
$link_login_only =  $_POST['link-login-only'];
$link_orig_esc =  $_POST['link-orig-esc'];
$mac_esc =  $_POST['mac-esc'];
$_SESSION['mac'] = $mac;
$_SESSION['ip'] = $ip;
$_SESSION['username'] = $username;
$_SESSION['link-login'] = $link_login;
$_SESSION['link-orig'] = $link_orig;
$_SESSION['error'] = $error;
$_SESSION['chap-id'] = $chap_id;
$_SESSION['chap-challenge'] = $chap_challenge;
$_SESSION['link-login-only'] = $link_login_only;
$_SESSION['link-orig-esc'] = $link_orig_esc;
$_SESSION['mac-esc'] = $mac_esc;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Wifi Puerto Montt - Regi&oacute;n de Los Lagos | soypuertomontt.cl</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700|Open+Sans:400,700" rel="stylesheet">
</head>
<body>
    <header>
        <div class="container">
            <a href="http://www.soychile.cl/puerto-montt/"><img src="http://www.soychile.cl/assets/escritorio/img/ciudades/27.png" alt="logo soypuerto-montt"></a>
            <img src="./img/logo-wifi.png" alt="">
        </div>
    </header>
    <section class="player">
        <h2>Esta publicidad ayuda a entregar este servicio</h2>
        <div class="embed-responsive">
            <div id="player"></div>
        </div>
        <h2>Faltan <span class="timer">X</span> para finalizar el video</h2>
        
    </section>
    <section class="content">
        <div class="container">
            <form action="redir.php" method="POST" id="leForm">
                <div class="form-group">
                    <label for="email">Por favor ingresa tu email</label>
                    <input type="email" name="email">
                </div>
                <div class="form-group">
                    <label for="motivo">Motivo del viaje</label>
                    <select name="motivo">
                        <option value="turismo-extranjero">Turismo (soy extranjero)</option>
                        <option value="turismo-chile">Turismo (soy chileno)</option>
                        <option value="negocios-extranjero">Negocios (soy extranjero)</option>
                        <option value="negocios-chile">Negocios (soy chileno)</option>
                    </select>
                </div>
                <?php
                if (isset($_POST['mac'])) { ?>
                <input type="hidden" name="mac" value="<?php echo $_POST['mac'];?>">
			    <input type="hidden" name="ip" value="<?php echo $_POST['ip'];?>">
			    <input type="hidden" name="username" value="<?php echo $_POST['username'];?>">
			    <input type="hidden" name="link-login" value="<?php echo $_POST['link-login'];?>">
			    <input type="hidden" name="link-orig" value="<?php echo $_POST['link-orig'];?>">
			    <input type="hidden" name="error" value="<?php echo $_POST['error'];?>">
			    <input type="hidden" name="chap-id" value="<?php echo $_POST['chap-id'];?>">
			    <input type="hidden" name="chap-challenge" value="<?php echo $_POST['chap-challenge'];?>">
			    <input type="hidden" name="link-login-only" value="<?php echo $_POST['link-login-only'];?>">
			    <input type="hidden" name="link-orig-esc" value="<?php echo $_POST['link-orig-esc'];?>">
			    <input type="hidden" name="mac-esc" value="<?php echo $_POST['mac-esc'];?>">
                <?php } ?>
                <button type="submit" >Continuar</button>
            </form>
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="img-wrapper"><img src="./img/logo-aeropuerto.png" alt=""></div>
        </div>
    </footer>
    <script src="js/main.js"></script>
</body>
</html>